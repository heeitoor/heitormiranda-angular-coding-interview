import { Component, OnInit } from "@angular/core";
import { QuestionsService } from "../../core/services/questions.service";
import { ActivatedRoute } from "@angular/router";
import { EMPTY, Subscription } from "rxjs";
import { catchError, switchMap, tap } from "rxjs/operators";
import { QuestionModel } from "../../core/state/question.model";

@Component({
  selector: "app-play",
  templateUrl: "./play.component.html",
  styleUrls: ["./play.component.scss"],
})
export class PlayComponent implements OnInit {
  questions: QuestionModel[] = [];
  questions$ = this.questionsService.questions$.pipe(
    tap((x) => (this.questions = x))
  );
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(
      switchMap((params) =>
        this.questionsService.getQuestions({
          type: params.type,
          amount: params.amount,
          difficulty: params.difficulty,
        })
      ),
      catchError((e) => {
        alert(`we were not able to get the questions`);

        return EMPTY;
      })
    )
    .subscribe();

  allAnswered = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService
  ) {}

  ngOnInit(): void {}

  onAnswerClicked(
    questionId: QuestionModel["_id"],
    answerSelected: string
  ): void {
    this.questionsService.selectAnswer(questionId, answerSelected);

    this.allAnswered =
      this.questions.filter((x) => x.selectedId).length ===
      this.questions.length;

    if (this.allAnswered) {
      const correctAnswers = this.questions.map(
        (x) => x.answers.find((y) => y.isCorrect)?._id
      );

      const selectedAnswers = this.questions.map((x) => x.selectedId);
      for (const answer of selectedAnswers) {
        if (correctAnswers.includes(answer)) {
          this.count++;
        }
      }
    }
  }

  count = 0;
}
